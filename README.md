# README #

This application simulates a car moving around a city. I implemented car/passenger movement, coordinate tracking, and the printing of coordinates (Tick(), VisitCoordinate() and PrintVisitedCoordinates() methods, respectively). I also added the ability to build a Race Car, to create different types of cars using the factory pattern, and the ability for the passenger to download the Veyo website on each tick. Each feature was added in subsequent commits to the master branch. The last commit also contains tests and some code cleanup.  

Tests were written using the NUnit Framework. 