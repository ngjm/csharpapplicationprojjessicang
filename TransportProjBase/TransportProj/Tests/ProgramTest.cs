﻿using System;

namespace TransportProj
{
	using NUnit.Framework;

	[TestFixture]
	public class ProgramTest
	{
		[Test]
		public void SedanTick()
		{
			const int cityLength = 10;
			const int cityWidth = 10;

			var city = new City(cityLength, cityWidth);
			Sedan sedan = new Sedan (0, 0, city, null);
			Passenger passenger = new Passenger(2, 2, 3, 4, city);

			Coordinate coordinate = Program.Tick (sedan, passenger);
			Assert.AreEqual(coordinate.XPos, 1);
			Assert.AreEqual(coordinate.YPos, 0);

			coordinate = Program.Tick (sedan, passenger);
			Assert.AreEqual(coordinate.XPos, 2);
			Assert.AreEqual(coordinate.YPos, 0);

			coordinate = Program.Tick (sedan, passenger);
			Assert.AreEqual(coordinate.XPos, 2);
			Assert.AreEqual(coordinate.YPos, 1);

			coordinate = Program.Tick (sedan, passenger);
			Assert.AreEqual(coordinate.XPos, 2);
			Assert.AreEqual(coordinate.YPos, 2);

			coordinate = Program.Tick (sedan, passenger);
			Assert.AreEqual(coordinate.XPos, 2);
			Assert.AreEqual(coordinate.YPos, 2);

			coordinate = Program.Tick (sedan, passenger);
			Assert.AreEqual(coordinate.XPos, 3);
			Assert.AreEqual(coordinate.YPos, 2);

			coordinate = Program.Tick (sedan, passenger);
			Assert.AreEqual(coordinate.XPos, 3);
			Assert.AreEqual(coordinate.YPos, 3);

			coordinate = Program.Tick (sedan, passenger);
			Assert.AreEqual(coordinate.XPos, 3);
			Assert.AreEqual(coordinate.YPos, 4);
		}

		[Test]
		public void RaceCarTick()
		{
			const int cityLength = 10;
			const int cityWidth = 10;

			var city = new City(cityLength, cityWidth);
			RaceCar raceCar = new RaceCar (0, 0, city, null);
			Passenger passenger = new Passenger(4, 5, 7, 7, city);

			Coordinate coordinate = Program.Tick (raceCar, passenger);
			Assert.AreEqual(coordinate.XPos, 2);
			Assert.AreEqual(coordinate.YPos, 0);

			coordinate = Program.Tick (raceCar, passenger);
			Assert.AreEqual(coordinate.XPos, 4);
			Assert.AreEqual(coordinate.YPos, 0);

			coordinate = Program.Tick (raceCar, passenger);
			Assert.AreEqual(coordinate.XPos, 4);
			Assert.AreEqual(coordinate.YPos, 2);

			coordinate = Program.Tick (raceCar, passenger);
			Assert.AreEqual(coordinate.XPos, 4);
			Assert.AreEqual(coordinate.YPos, 4);

			coordinate = Program.Tick (raceCar, passenger);
			Assert.AreEqual(coordinate.XPos, 4);
			Assert.AreEqual(coordinate.YPos, 5);

			coordinate = Program.Tick (raceCar, passenger);
			Assert.AreEqual(coordinate.XPos, 4);
			Assert.AreEqual(coordinate.YPos, 5);

			coordinate = Program.Tick (raceCar, passenger);
			Assert.AreEqual(coordinate.XPos, 6);
			Assert.AreEqual(coordinate.YPos, 5);

			coordinate = Program.Tick (raceCar, passenger);
			Assert.AreEqual(coordinate.XPos, 7);
			Assert.AreEqual(coordinate.YPos, 5);

			coordinate = Program.Tick (raceCar, passenger);
			Assert.AreEqual(coordinate.XPos, 7);
			Assert.AreEqual(coordinate.YPos, 7);
		}

		[Test]
		public void SmartCarTick()
		{
			const int cityLength = 10;
			const int cityWidth = 10;

			var city = new City(cityLength, cityWidth);
			SmartCar smartCar = new SmartCar (0, 0, city, null);
			Passenger passenger = new Passenger(5, 4, 7, 7, city);

			Coordinate coordinate = Program.Tick (smartCar, passenger);
			Assert.AreEqual(coordinate.XPos, 3);
			Assert.AreEqual(coordinate.YPos, 0);

			coordinate = Program.Tick (smartCar, passenger);
			Assert.AreEqual(coordinate.XPos, 5);
			Assert.AreEqual(coordinate.YPos, 0);

			coordinate = Program.Tick (smartCar, passenger);
			Assert.AreEqual(coordinate.XPos, 5);
			Assert.AreEqual(coordinate.YPos, 3);

			coordinate = Program.Tick (smartCar, passenger);
			Assert.AreEqual(coordinate.XPos, 5);
			Assert.AreEqual(coordinate.YPos, 4);

			coordinate = Program.Tick (smartCar, passenger);
			Assert.AreEqual(coordinate.XPos, 5);
			Assert.AreEqual(coordinate.YPos, 4);

			coordinate = Program.Tick (smartCar, passenger);
			Assert.AreEqual(coordinate.XPos, 7);
			Assert.AreEqual(coordinate.YPos, 4);

			coordinate = Program.Tick (smartCar, passenger);
			Assert.AreEqual(coordinate.XPos, 7);
			Assert.AreEqual(coordinate.YPos, 7);
		}
	}
}

