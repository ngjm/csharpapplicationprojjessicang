﻿using System;

namespace TransportProj
{

	using NUnit.Framework;

	[TestFixture]
	public class CarFactoryTest
	{
		[Test]
		public void SmartCarTest ()
		{
			const int cityLength = 10;
			const int cityWidth = 10;

			var city = new City(cityLength, cityWidth);
			Car theCar = CarFactory.CreateAndReturnCar(3, 2, 2, city, null);

			Assert.IsInstanceOf<SmartCar>(theCar);
		}

		[Test]
		public void RaceCarTest ()
		{
			const int cityLength = 10;
			const int cityWidth = 10;

			var city = new City(cityLength, cityWidth);
			Car theCar = CarFactory.CreateAndReturnCar(1, 2, 2, city, null);

			Assert.IsInstanceOf<RaceCar>(theCar);
		}

		[Test]
		public void DefaultTest ()
		{
			const int cityLength = 10;
			const int cityWidth = 10;

			var city = new City(cityLength, cityWidth);
			Car theCar = CarFactory.CreateAndReturnCar(12, 2, 2, city, null);

			Assert.IsInstanceOf<Sedan>(theCar);
		}
	}
}

