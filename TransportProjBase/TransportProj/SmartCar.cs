﻿using System;

namespace TransportProj
{
    public class SmartCar : Car
    {
		private static int RegularSpeed = 3;

		public SmartCar(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
			this.speed = RegularSpeed;
        }

		public override void MoveUp()
		{
			if (YPos < City.YMax)
			{
				YPos += speed;
				WritePositionToConsole();
				ResumeSpeed ();
			}
		}

		public override void MoveDown()
		{
			if (YPos > 0)
			{
				YPos -= speed;
				WritePositionToConsole();
				ResumeSpeed ();
			}
		}

		public override void MoveRight()
		{
			if (XPos < City.XMax)
			{
				XPos += speed;
				WritePositionToConsole();
				ResumeSpeed ();
			}
		}

		public override void MoveLeft()
		{
			if (XPos > 0)
			{
				XPos -= speed;
				WritePositionToConsole();
				ResumeSpeed ();
			}
		}

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("SmartCar moved to x - {0} y - {1}", XPos, YPos));
        }

		public override void ResumeSpeed() {
			if (this.speed < RegularSpeed) {
				this.speed = RegularSpeed;
			}
		}
    }
}
