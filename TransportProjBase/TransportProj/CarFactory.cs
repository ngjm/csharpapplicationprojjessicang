﻿using System;

namespace TransportProj
{
	public class CarFactory
	{
		static public Car CreateAndReturnCar(int choice, int x, int y, City city, Passenger p) {
		
			Car theCar = null;
			switch (choice) {
			case 1: 
				theCar = new RaceCar (x, y, city, p);
				break;
			case 2:
				theCar = new Sedan (x, y, city, p);
				break;
			case 3: 
				theCar = new SmartCar (x, y, city, p);
				break;
			default:
				theCar = new Sedan (x, y, city, p);
				break;
			}

			return theCar;
		}
	}
}

