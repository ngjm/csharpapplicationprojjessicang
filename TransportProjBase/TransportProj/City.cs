﻿using System;
namespace TransportProj
{
    public class City
    {
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
        }

        public Car AddCarToCity(int xPos, int yPos)
        {
			//RaceCar car = new RaceCar(xPos, yPos, this, null);
			var rand = new Random();
			Car car =  CarFactory.CreateAndReturnCar(rand.Next(1,4), xPos, yPos, this, null);
			Console.WriteLine(car.GetType().ToString());
            return car;
        }

        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Passenger passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);

            return passenger;
        }

    }
}
