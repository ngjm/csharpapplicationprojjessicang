﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;

namespace TransportProj
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var rand = new Random();
            const int cityLength = 10;
            const int cityWidth = 10;

            var city = new City(cityLength, cityWidth);
            var car = city.AddCarToCity(rand.Next(cityLength - 1), rand.Next(cityWidth - 1));

			Console.WriteLine(String.Format("Sedan at  x - {0} y - {1}", car.XPos, car.YPos));
            var passenger = city.AddPassengerToCity(rand.Next(cityLength - 1), rand.Next(cityWidth - 1), rand.Next(cityLength - 1), rand.Next(cityWidth - 1));

			Console.WriteLine(String.Format("Passenger at  x - {0} y - {1}", passenger.GetCurrentXPos(), passenger.GetCurrentYPos()));

			Console.WriteLine(String.Format("Destination at  x - {0} y - {1}", passenger.DestinationXPos, passenger.DestinationYPos));

			Console.WriteLine ("----------------------------------");
            // TODO -- REQUIRED: instantiate an appropriate data structure that can be used to store Coordinates. 
            // Ensure the data structure you pick will allow for a time efficient solution.
            //ICollection<Coordinate> visitedCoordinates = null; 

			IDictionary<Coordinate, int> visitedCoordinates = new Dictionary<Coordinate, int>();
            while(!passenger.IsAtDestination())
            {
                var currentCoordinate = Tick(car, passenger);
                VisitCoordinate(currentCoordinate, visitedCoordinates);

				Task.Run(async () => MainAsync()).GetAwaiter().GetResult();


				/*
				Task.Run(async () => {
						var client = new WebClient();
						client.DownloadString("http://www.veyo.com");
						Console.WriteLine("Called veyo");
				}).GetAwaiter().GetResult();
				*/


            }

            PrintVisitedCoordinates(visitedCoordinates);
        }

		static async Task MainAsync()
		{
			var client = new WebClient();
			client.DownloadString("http://www.veyo.com");
			Console.WriteLine("Visited veyo.com"); 
		}


        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        /// <returns>A Coordinate representing the location of the Car after the move was made</returns>
        public static Coordinate Tick(Car car, Passenger passenger)
        {
            // TODO -- REQUIRED: fill this method in.
			bool isAtXPass = (car.XPos == passenger.GetCurrentXPos ());
			bool isAtYPass = (car.YPos == passenger.GetCurrentYPos ());

			if (car.Passenger == null && isAtXPass && isAtYPass) {
				// car does not have passenger, but is at coordinate of passenger
				passenger.GetInCar (car);
			} else {
				if (car.Passenger == null) {
					// car does not have passenger, move car to passenger 
					if (car.XPos != passenger.GetCurrentXPos ()) {

						// if speed is faster than distance to passenger, then slow down
						if (car.speed > Math.Abs (car.XPos - passenger.GetCurrentXPos ())) {
							car.speed = Math.Abs (car.XPos - passenger.GetCurrentXPos ());
						}
					
						if (car.XPos > passenger.GetCurrentXPos ()) {
							car.MoveLeft ();
						} else {
							car.MoveRight ();
						}
					} else if (car.YPos != passenger.GetCurrentYPos ()) {

						if (car.speed > Math.Abs (car.YPos - passenger.GetCurrentYPos ())) {
							car.speed = Math.Abs (car.YPos - passenger.GetCurrentYPos ());
						}

						if (car.YPos > passenger.GetCurrentYPos ()) {
							car.MoveDown ();
						} else {
							car.MoveUp ();
						}
					}
				} else {
					// car has passenger, move car to destination
					if (car.XPos != passenger.DestinationXPos) {

						// if speed is faster than distance to destination, then slow down
						if (car.speed > Math.Abs (car.XPos - passenger.DestinationXPos)) {
							car.speed = Math.Abs (car.XPos - passenger.DestinationXPos);
						}

						if (car.XPos > passenger.DestinationXPos) {
							car.MoveLeft ();
						} else {
							car.MoveRight ();
						}
					} else if (car.YPos != passenger.DestinationYPos) {
						if (car.speed > Math.Abs (car.YPos - passenger.DestinationYPos)) {
							car.speed = Math.Abs (car.YPos - passenger.DestinationYPos);
						}

						if (car.YPos > passenger.DestinationYPos) {
							car.MoveDown ();
						} else {
							car.MoveUp ();
						}
					}

				}
			}
			var coord = new Coordinate ();
			coord.XPos = car.XPos;
			coord.YPos = car.YPos;
			return coord;
		}

        /// <summary>
        /// Visits a coordinate by adding it to the visitedCoordinates collection.
        /// </summary>
        /// <param name="coordinate">the coordinate to visit</param>
        /// <param name="visitedCoordinates">the collection of coordinates that were already visited</param>
		public static void VisitCoordinate(Coordinate coordinate, IDictionary<Coordinate, int> visitedCoordinates) 
        {
            // TODO -- REQUIRED: fill this method in.

			int val;
			if (visitedCoordinates.TryGetValue(coordinate, out val)) {
				visitedCoordinates[coordinate] = val + 1;
			}
			else {
				visitedCoordinates.Add(coordinate, 1);
			}
		
            // TODO -- REQUIRED: Leave a comment at the end of the method explaining what the time complexity of this method is (using Big O notation).

			// Get and check value of key in one access to dictionary, adding to dictionary is constant time

			// Per https://msdn.microsoft.com/en-us/library/xfhwa508.aspx
			// "Retrieving a value by using its key is very fast, close to O(1), because the Dictionary<TKey, TValue> class is implemented as a hash table." 

			// Total time complexity is constant, O(1)
        }

        /// <summary>
        /// Prints all the coordinates that were visited and the number of times each coordinate was visited to the Console.
        /// For example, if the coordinate (1, 1) was visited once and the Coordinate (1, 2) was visited twice, the output should be as follows:
        /// 
        /// Visited coordinates:
        /// (1, 1) - 1
        /// (1, 2) - 2
        /// 
        /// </summary>
        /// <param name="visitedCoordinates">The collection of coordinates that were visited</param>
        private static void PrintVisitedCoordinates(IDictionary<Coordinate, int> visitedCoordinates)
        {
            // TODO -- REQUIRED: fill this method in.

			// When you use foreach to enumerate dictionary elements, the elements are retrieved as KeyValuePair objects.
			foreach (KeyValuePair<Coordinate, int> kvp in visitedCoordinates) {
				Console.WriteLine("({0}, {1}) - {2}", kvp.Key.XPos, kvp.Key.YPos, kvp.Value);
			}

            // TODO -- REQUIRED: Leave a comment at the end of the method explaining what the time complexity of this method is (using Big O notation).
			// Complexity of a for loop is O(n), where n is the total number of KeyValuePairs. 
        }
    }
}
