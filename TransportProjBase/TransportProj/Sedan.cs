﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {
		private static int RegularSpeed = 1;

        public Sedan(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
			this.speed = RegularSpeed;
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos++;
                WritePositionToConsole();
				ResumeSpeed ();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0)
            {
                YPos--;
                WritePositionToConsole();
				ResumeSpeed ();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos++;
                WritePositionToConsole();
				ResumeSpeed ();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0)
            {
                XPos--;
                WritePositionToConsole();
				ResumeSpeed ();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", XPos, YPos));
        }

		public override void ResumeSpeed() {
			if (this.speed < RegularSpeed) {
				this.speed = RegularSpeed;
			}
		}
    }
}
